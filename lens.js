var WIDTH = $(window).width();
var HEIGHT = $(window).height();
theCanvas = undefined;
ctx = undefined;

lenses = [];
NUM_LENSES = 0;

sp = 40;
y = 60;

last_time = 0;

options = {};
options.draw_rays = true;
options.draw_global = false;

var addEvent = function(elem, type, eventHandle) {
    if (elem == null || typeof(elem) == 'undefined') return;
    if ( elem.addEventListener ) {
        elem.addEventListener( type, eventHandle, false );
    } else if ( elem.attachEvent ) {
        elem.attachEvent( "on" + type, eventHandle );
    } else {
        elem["on"+type]=eventHandle;
    }
};
var resize_timer;
addEvent(window, "resize", function() {
    clearTimeout(resize_timer);
    resize_timer = setTimeout(function() {
        WIDTH = $(window).width();
        HEIGHT = $(window).height();
        set_add_button();
        window.requestAnimationFrame(run);
    }, 200); // redraw only every 200ms
});

function max(a, b) {
    if (a > b) {
        return a;
    }
    return b;
}
function min(a, b) {
    if (a < b) {
        return a;
    }
    return b;
}

///////////////
// Assorted hit tests for various objects
//  could be refactored into a generalized hit_entity, but would require specialization anyway
///////////////
function hit_lens(l, x, y) {
    var tol = 1.0;
    var r = get_r(l.n, l.f);
    r = Math.abs(r);
    var d = (r - Math.sqrt(r*r - l.h*l.h));
    if (Math.abs(l.l - x) - max(10, d) < tol && Math.abs(HEIGHT/2-y) < l.h) {
        return true;
    }
    return false;
}
function hit_above_lens(l, x, y) {
    var tol = 20.0;
    var r = get_r(l.n, l.f);
    r = Math.abs(r);
    var d = min((r - Math.sqrt(r*r - l.h*l.h)),30);
    if (Math.abs(l.l - x) - max(10, d) < tol && Math.abs(HEIGHT/2-l.h-y) < tol) {
        return true;
    }
    return false;
}
function hit_lens_focus(l, x, y) {
    var tol = 20.0;
    var d = Math.abs((l.l - l.f) - x);
    if (d < tol && Math.abs(HEIGHT/2 - y) < tol) {
        return d;
    }
    return 100; // > tol
}
function hit_object(l, x, y) {
    var tol = 20.0;
    var hh = HEIGHT/2;
    var isabove = (l.y > hh) ? -1 : 1;
    var ycheck = isabove*(HEIGHT/2 - y) < l.y && isabove ? (y < hh) : (y > hh);
    if (Math.abs((l.sp) - x) < tol && ycheck) {
        return true;
    }
    return false;
}
function hit_image(l, x, y) {
    var tol = 20.0;
    var d = Math.abs((l.l + l.s2) - x);
    if (d < tol ) {
        return d;
    }
    return 100; // > tol
}

dragging = false;
dragging_lens = -1;
dragging_focus = -1;
hover_lens = -1;
hover_arrow = -1;
hover_delete = -1;
//dragging_object = -1;

// get lowest index lens that's under the mouse when we click
function get_hit_lenses(x, y) {
    for (var i = 0; i < NUM_LENSES; i++) {
        if (hit_lens(lenses[i], x, y)) {
            return i;
        }
    }
    return -1;
}

// get lowest index object/image arrow that's under the mouse when we click
function get_hit_arrows(x, y) {
    // check initial object first
    if (hit_object(lenses[0], x, y)) {
        return -2;
    }
    var best = 100;
    var besti = -1;
    for (var i = 0; i < NUM_LENSES; i++) {
        var t = hit_image(lenses[i], x, y);
        if (t < best) {
            best = t;
            besti = i;
        }
    }
    return besti;
}
// test if we hit a delete lens button
function get_hit_delete(x, y, del) {
    del = del || false;
    var worked = false;
    var hit = -1;
    for (var i = 0; i < NUM_LENSES; i++) {
        if (hit_above_lens(lenses[i], x, y)) {
            hit = i;
            if (del) {
                remove_lens(i);
                worked = true;
            }

        }
    }
    if (worked) {
        window.requestAnimationFrame(run);
    }
    return hit;
    //return worked;
}

// returns closes focus being clicked on
function get_hit_focuses(x, y) {
    var best = 100;
    var besti = -1;
    for (var i = 0; i < NUM_LENSES; i++) {
        var t = hit_lens_focus(lenses[i], x, y);
        if (t < best) {
            best = t;
            besti = i;
        }
    }
    return besti;
}
add_button = {};
add_button.rx = 50;
add_button.ry = 100;
add_button.x = WIDTH - add_button.rx;
add_button.y = HEIGHT/2 - 0.5*add_button.ry;
function set_add_button() {
    add_button.x = WIDTH - add_button.rx;
    add_button.y = HEIGHT/2 - 0.5*add_button.ry;
}

// create and initialize a new lens
function new_lens(x, f) {
    f = f || 40;
    l = {}
    l.l = x;
    l.f = f;
    l.n = 8.00
    l.h = 200;
    l.s = 400;
    l.sp = l.l - l.s;
    l.y = 60;
    l.s2 = 0;
    l.y2 = 0;
    l.num = NUM_LENSES;
    return l;
}
function check_add_button(x, y) {
    if (Math.abs(x - add_button.x) < add_button.rx && Math.abs(y - add_button.y) < add_button.ry) {
        var x = add_button.x - add_button.rx*2;
        lenses[NUM_LENSES] = new_lens(x);
        NUM_LENSES+=1;
        window.requestAnimationFrame(run);
        return true;
    }
    return false;
}
function remove_lens(ri) {
    lenses.splice(ri, 1);
    NUM_LENSES--;
    for (var i = 0; i < NUM_LENSES; i++) {
        lenses[i].num = i;
    }
}

/////////////////////////
// event listeners
/////////////////////////
function mouseMoveListener(e) {
    var mx = e.clientX;
    var my = e.clientY;
    //sp = mx;
    //y = HEIGHT/2 - my;
    window.requestAnimationFrame(run);
    hover_lens = get_hit_lenses(mx, my);
    hover_arrow = get_hit_arrows(mx, my);
    hover_delete = get_hit_delete(mx, my);

    if ( ! dragging) {
        return;
    }
    if (dragging_focus > -1) {
        lenses[dragging_focus].f = lenses[dragging_focus].l - mx;
    } else if (dragging_lens > -1) {
        lenses[dragging_lens].l = mx;
    } else { // assume we're dragging the object
        sp = mx;
        y = HEIGHT/2 - my;
    }
}
function mouseDownListener(e) {
    window.addEventListener("mouseup", mouseUpListener);

    var x = e.clientX;
    var y = e.clientY;
    if (! check_add_button(x, y) && get_hit_delete(x, y, true) == -1) {
        dragging_lens = get_hit_lenses(x, y);
        dragging_focus = get_hit_focuses(x, y);
        if (dragging_focus > -1 || dragging_lens > -1 || hit_object(lenses[0], x, y)) {
            dragging = true;
        }
    }

    window.removeEventListener("mousedown", mouseDownListener, false);
    window.addEventListener("mouseup", mouseUpListener, false);

    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
    return false;
}
function mouseUpListener(e) {
    window.addEventListener("mousedown", mouseDownListener, false);
    window.removeEventListener("mouseup", mouseUpListener, false);
    if (dragging) {
        dragging = false;
    }
}



//////////////////////
//// Entry point: ////
//////////////////////
$(document).ready(function() {
    // set up some lenses
    lenses[NUM_LENSES] = new_lens(600, 70);
    NUM_LENSES+=1;
    lenses[NUM_LENSES] = new_lens(650, -100);
    NUM_LENSES+=1;
    lenses[NUM_LENSES] = new_lens(700, 80);
    NUM_LENSES+=1;
    lenses[NUM_LENSES] = new_lens(950, 100);
    NUM_LENSES+=1;

    theCanvas = document.getElementById("myCanvas");
    ctx = theCanvas.getContext("2d");
    window.addEventListener("mousedown", mouseDownListener, false);
    window.addEventListener("mousemove", mouseMoveListener);
    run();

    //window.addEventListener("keypress", keypressListener);
});

// handle keyboard input
$(document).keydown(function(e) {
    //console.log(e.which);
    var moveby = event.shiftKey ? 10 : 1;
    switch(e.which) {
        case 76: // l
            options.draw_rays = ! options.draw_rays;
            break;
        case 73: // i
            options.draw_global = ! options.draw_global;
            break;
        case 37: // left
            sp -= moveby;
            break;
        case 38: // up
            y += moveby;
            break;
        case 39: // right
            sp += moveby;
            break;
        case 40: // down
            y -= moveby;
            break;
        default: return;
    }
    e.preventDefault();
    run();
});

// recalculate the current frame
// and display
function run() {
    update();
    theCanvas.width = WIDTH;
    theCanvas.height = HEIGHT;
    clear(theCanvas);
    draw();
}


// calculate image position
function get_s2(s, f) {
    return 1.0 / ((1.0/f) - (1.0/s));
}

// calculate magnification
function get_m(s, s2) {
    return (-1 * s2) / s;
}
// those last two one-line functions were the only physics necessary for this whole thing

// returns radius used to draw lens
function get_r(n, f) {
    return (n - 1.0)*f*2;
}

function update_lens(l) {
    l.s2 = get_s2(l.s, l.f);
    l.m = get_m(l.s, l.s2);
    l.y2 = l.y * l.m;
    l.s2p = l.l + l.s2;
}

// update indices while dragging
function update_dragging_indices(i, j) {
    if (dragging_lens == i) {
        dragging_lens = j;
    } else if (dragging_lens == j) {
        dragging_lens = i;
    }
}

// swap lens positions in list
function swap_lenses(i, j) {
    var temp = lenses[j];
    lenses[j] = lenses[i];
    lenses[i] = temp;
    update_dragging_indices(i, j);
}

// update each lens's image
// starting with first lens, cascading down
function update() {
    // get user input
    lenses[0].sp = sp;
    lenses[0].y = y;

    // reset indices
    for (var i = 0; i < NUM_LENSES; i++) {
        lenses[i].num = i;
    }

    // keep the lenses in sorted order by x value
    // insertion sort because we only have a small number of lenses
    // reimplemented because we need a custom swap function
    for (var i = 1; i < NUM_LENSES; i++) {
        var j = i;
        while (j > 0 && lenses[j-1].l > lenses[j].l) {
            swap_lenses(j-1, j);
            j--;
        }
    }

    // join adjacent lenses by image->object
    for (var i = 0; i < NUM_LENSES; i++) {
        if (i == 0) {
            // get first lens's image and height from the global
            lenses[i].s = lenses[i].l - sp;
            lenses[i].sp = sp;
            lenses[i].y = y;
        } else {
            // swap from lens1 img coords to lens2 obj coords
            var d = lenses[i].l - lenses[i-1].l;
            lenses[i].s = d - lenses[i-1].s2;
            lenses[i].y = lenses[i-1].y2;
            lenses[i].sp = lenses[i-1].l - lenses[i-1].s2;
        }
        update_lens(lenses[i]);
    }
}

// draw button to add a new lens
function draw_add_button() {
    ctx.fillStyle = "#228866";
    ctx.strokeStyle = "#002200";
    ctx.globalAlpha = 0.4;
    ctx.fillRect(add_button.x,add_button.y,add_button.rx,add_button.ry);
    ctx.globalAlpha = 1.0;
    ctx.strokeRect(add_button.x,add_button.y,add_button.rx,add_button.ry);
}

// draw button to remove a given lens
function draw_remove_button(x,y) {
    ctx.fillStyle = "#CC2244";
    ctx.strokeStyle = "#550000";
    var rx = 60;
    var ry = 40;
    ctx.globalAlpha = 0.4;
    ctx.fillRect(x,y,rx,ry);
    ctx.globalAlpha = 1.0;
    ctx.strokeRect(x,y,rx,ry);
}

// draw some debuggin info
function draw_global_info() {
    if ( ! options.draw_global) return;
    ctx.font="20px verdana";
    ctx.fillStyle="#FFFFFF";
    ctx.fillText("Dragging lens: " + dragging_lens, 20, 40);
    ctx.fillText("Dragging focus: " + dragging_focus, 20, 60);
    ctx.fillText("Hover lens: " + hover_lens, 20, 80);
    ctx.fillText("Hover arrow: " + hover_arrow, 20, 100);
    ctx.fillText("Hover delete: " + hover_delete, 20, 120);

}

// return increasingly bright gray tones
// so I don't repeat colors everywhere
function get_gray(b) {
    if (b == 0) return "#666";
    if (b == 1) return "#BBB";
    if (b == 2) return "#DDD";
    if (b == 3) return "#FFF";
    return "#000";
}

// draw one lens
function draw_l(l, hh) {
    //// draw lens ////
    var do_arrow_representation = false;
    if (do_arrow_representation) {
        var flip = l.f < 0;
        arrow(l.l, hh, l.l, hh + l.h, undefined, flip);
        arrow(l.l, hh, l.l, hh - l.h, undefined, flip);
    } else {
        draw_lens(l.l, hh, l.f, l.h, l.n);
    }
    // draw focii
    point(l.l-l.f, hh, 5);
    point(l.l+l.f, hh, 5, "#333"); // false focus

    //// draw object ////
    if (l.num == 0) { // only if first object
        var b = 2;
        if (hover_arrow == -2 || (dragging && dragging_lens == -1 && dragging_focus == -1))  {
            // hovering on first object
            // part after || fixes not showing problem when dragging
            b++;
            draw_text_object(l.sp, hh);

        }
        arrow(l.sp, hh, l.sp, hh - l.y, get_gray(b));
    }

    if (hover_delete == l.num) {
        draw_remove_button(l.l - 30, hh - l.h - 20);
    }

    //// draw image ////
    var b = 0;
    if (l.num == NUM_LENSES-1) {
        b++;
    }
    if (hover_arrow == l.num) { // hovering on this image
        b++;
        //// draw image identifier ////
        ctx.font="20px verdana";
        ctx.fillStyle="#FFFFFF";
        var ah = 10 - (10.0 / ((Math.abs(l.y2)/10.0) + 2));
        ctx.fillText("" + l.num, l.s2p + 15, hh - l.y2 + ah);
    }
    arrow(l.s2p, hh, l.s2p, hh - l.y2, get_gray(b));


    //// draw info text ////
    if (hover_lens == l.num) {
        draw_text_lens(l, l.l, hh + l.h + 10);
        // draw lens number
        ctx.fillText("" + l.num, l.l-7,  hh - l.h - 15);
    }

    if ( ! options.draw_rays) return;
    //// draw principle rays ////
    var pr_alpha = 0.3
    var RED = "#00AAAA"
    var GREEN = "#00FFAA"
    var BLUE = "#00AAFF"
    // through center
    //   tip of object to tip of img, always draw
    var l0;
    if (l.num > 0) {
        l0 = lenses[l.num-1];
    } else {
        l0 = {};
        l0.sp = sp;
        l0.y = y;
    }
    //extended_line(l.sp, hh - l.y, l.s2p, hh - l.y2, "#FF0000", pr_alpha);
    var dotted  = l.s2 < 0;
    var virtual = l.s < 0;
    //line((l.l - l.s), hh - l.y, (l.l + l.s2), hh - l.y2, RED, pr_alpha, dotted);
    line((l.l - l.s), hh - l.y, l.l, hh, RED, pr_alpha, virtual);
    line(l.l, hh, (l.l + l.s2), hh - l.y2, RED, pr_alpha, dotted);
    // parallel
    if (Math.abs(l.y) < l.h) {
        //    tip of object to lens
        line((l.l - l.s), hh - l.y, l.l, hh - l.y, GREEN, pr_alpha, virtual);
        //    lens to tip of img
        //extended_line(l.l, hh - l.y, l.s2p, hh - l.y2, "#00FF00", pr_alpha);
        line(l.l, hh - l.y, l.s2p, hh - l.y2, GREEN, pr_alpha, dotted);
    }
    if (Math.abs(l.y2) < l.h) {
        //    tip of object to lens
        line((l.l - l.s), hh - l.y, l.l, hh - l.y2, BLUE, pr_alpha, virtual);
        //    tip of object to bottom of lens
        //extended_line(l.l, hh - l.y2, l.s2p, hh - l.y2, "#0000FF", pr_alpha);
        line(l.l, hh - l.y2, l.s2p, hh - l.y2, BLUE, pr_alpha, dotted);
    }
}

// draw all debug info per lens
function draw_text_lens(l, x0, y0) {
    x0 = x0 || 0;
    y0 = y0 || 0;
    var dx = -40
    ctx.font="20px verdana";
    ctx.fillStyle="#FFFFFF";
    var prec = 1;
    ctx.fillText("f:  " + l.f.toFixed(prec),x0+dx,y0+20);
    ctx.fillText("s:  " + l.s.toFixed(prec),x0+dx,y0+40);
    if (l.s2 < 0) {
        ctx.fillText("s': " + l.s2.toFixed(prec) + "   Virtual",x0+dx,y0+60);
    } else {
        ctx.fillText("s': " + l.s2.toFixed(prec) + "   Real",   x0+dx,y0+60);
    }
    var m = -1 * l.s2 / l.s
    if (m < 0) {
        ctx.fillText("m: " +  m.toFixed(prec) + "   Inverted",x0+dx,y0+80);
    } else {
        ctx.fillText("m: " + m.toFixed(prec) + "   Upright"  ,x0+dx,y0+80);
    }
    ctx.fillText("y:  " + l.y.toFixed(prec),  x0+dx,y0+100);
    ctx.fillText("y': " + l.y2.toFixed(prec), x0+dx,y0+120);
    ctx.fillText("l:  " + l.l.toFixed(prec),  x0+dx,y0+140);
}
function draw_text_object(x0, y0) {
    x0 = x0 || 0;
    y0 = y0 || 0;
    ctx.font="20px verdana";
    ctx.fillStyle="#FFFFFF";
    var prec = 1;
    ctx.fillText("sp: " + x0.toFixed(prec),x0-30,y0);
}

// global draw function, calls draw on each lens and draws global info
function draw() {
    var hh = HEIGHT/2;
    line(0, hh, WIDTH, hh, "#888");
    draw_add_button();

    draw_global_info();

    for (var i = 0; i < NUM_LENSES; i++) {
        draw_l(lenses[i], hh);
    }
}

function inside(x, y) {
    return (x > 0 && y > 0 && x < WIDTH && y < HEIGHT);

}

// Assorted drawing routines for lines, dashed lines, points, etc
function line(x1, y1, x2, y2, stroke, alpha, dotted) {
    if ( ! isFinite(x1) || ! isFinite(x2) || ! isFinite(y1) || ! isFinite(y2))
        return;
    dotted = dotted || false;
    stroke = stroke || "#EEE";
    alpha = alpha || 1.0;
    ctx.globalAlpha = alpha;
    ctx.strokeStyle = stroke;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    if ( ! dotted) {
        ctx.lineTo(x2, y2);
    } else {
        var dashLen = 4;
        var dX = x2 - x1;
        var dY = y2 - y1;
        var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
        var dashX = dX / dashes;
        var dashY = dY / dashes;

        var q = 0;
        while (q++ < dashes) {
            x1 += dashX;
            y1 += dashY;
            ctx[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
        }
        ctx[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
    }
    ctx.stroke();
    ctx.closePath();
    ctx.globalAlpha = 1.0;
}

CanvasRenderingContext2D.prototype.dashedLine = function (x1, y1, x2, y2, dashLen) {
    if (dashLen == undefined) dashLen = 2;
    this.moveTo(x1, y1);

    var dX = x2 - x1;
    var dY = y2 - y1;
    var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
    var dashX = dX / dashes;
    var dashY = dY / dashes;

    var q = 0;
    while (q++ < dashes) {
        x1 += dashX;
        y1 += dashY;
        this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
    }
    this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
};
function extended_line(x1, y1, x2, y2, stroke, alpha) {
    stroke = stroke || "#EEE";
    alpha = alpha || 1.0;
    ctx.globalAlpha = alpha;
    ctx.strokeStyle = stroke;
    ctx.beginPath();
    var x3 = x2 + WIDTH;
    var dydx = 0;
    if (x2 - x1 != 0) {
        dydx = (y2 - y1) / (x2 - x1);
    }
    var y3 = (x3-x2) * (dydx) + y2;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x3, y3);
    ctx.stroke();
    ctx.closePath();
    ctx.globalAlpha = 1.0;
}
function arrow(x1, y1, x2, y2, stroke, flip) {
    if (x1 < -10 || x1 > WIDTH+10) return;
    if (x2 < -10 || x2 > WIDTH+10) return;
    stroke = stroke || "#EEE";
    flip = flip || false;
    ctx.strokeStyle = stroke;
    ctx.beginPath();

    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();

    // make arrow height approach 0 at axis
    var dy = (y2 - y1);
    var ah = 10 - (10.0 / ((Math.abs(dy)/10.0) + 1));
    if (flip) {
        ah *= -1;
    }
    var dx = 10;
    if (y2 < y1) {
        ah *= -1;
    }
    ctx.moveTo(x2, y2);
    ctx.lineTo(x2-dx, y2-ah);
    ctx.stroke();
    ctx.moveTo(x2, y2);
    ctx.lineTo(x2+dx, y2-ah);
    ctx.stroke();
    ctx.closePath();

}
function point (x, y, r, fill) {
    r = r || 3;
    fill = fill || "#DDD";
    ctx.fillStyle = fill;
    var r2 = r / 2.0;
    ctx.fillRect(x-r2,y-r2,r,r);
}

// draw lens using two arcs
// figures out concave/convex and radius
function draw_lens(cx, cy, f, h, n, stroke, fill, linewidth) {
    stroke = stroke || "#EEE";
    fill = fill || "#ABCDEF"; // light blue
    ctx.strokeStyle = stroke;
    ctx.fillStyle = fill;
    linewidth = linewidth || 2;
    var r = (n - 1.0)*f*2;
    var r = get_r(n, f);
    if (r > 0 && r < h) r = h;
    if (r < 0 && Math.abs(r) < h) r = -1 * h;
    var dir = r < 0;
    r = Math.abs(r);
    var d = Math.sqrt(r*r - ((h)*(h)));

    ctx.beginPath();
    ctx.lineWidth = linewidth;
    if ( ! dir) { // convex
        var theta1 = Math.asin(h / (1.0*r));
        var theta2 = Math.asin(h / (-1.0*r));
        ctx.arc(cx-d, cy, r, theta1, theta2, true);
        var theta3 = Math.asin(h / (1.0*r)) + Math.PI;
        var theta4 = Math.asin(h / (-1.0*r)) + Math.PI;
        ctx.arc(cx+d, cy, r, theta3, theta4, true);
        ctx.stroke();
        ctx.globalAlpha = 0.5;
        ctx.fill();
        ctx.globalAlpha = 1.0;
    } else { // concave
        var w = 5; // inner width
        var d2 = r + w;
        var theta1 = Math.asin(h / (1.0*r));
        var theta2 = Math.asin(h / (-1.0*r));
        ctx.arc(cx - d2, cy, r, theta1, theta2, true);
        var theta3 = Math.asin(h / (1.0*r)) + Math.PI;
        var theta4 = Math.asin(h / (-1.0*r)) + Math.PI;
        ctx.arc(cx + d2, cy, r, theta3, theta4, true);
        // draw bottom line
        // top line was already drawn connecting arc() calls
        ctx.lineTo(cx - r-r*Math.cos(theta4) - w, cy+h);
        ctx.stroke();
        ctx.globalAlpha = 0.5; // fill with alpha
        ctx.fill();
        ctx.globalAlpha = 1.0;
    }
    ctx.closePath();
}

// by assigning the width we force a clear screen
// doesn't actually change anything because we assign it itself
function clear(canv) {
    var canvwidth = canv.width;
    canv.width = canvwidth;
}
